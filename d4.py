import copy

file = open("input/d4.txt")
lines = file.readlines()
file.close()

class Position:
    def __init__(self, board_id, x_id, y_id, value):
        self.board_id = board_id
        self.x_id = x_id
        self.y_id = y_id
        self.value = value
        self.marked = False

def parse_input(lines):
    numbers = lines.pop(0).split(',')
    numbers = [int(i) for i in numbers]
    lines.pop(0)

    # boards = {}
    positions = []
    board_number = 0
    # position_number = 0
    for i,line in enumerate(lines):
        board_row = i % 6
        if board_row != 5:
            line_board_numbers = line.split()
            line_board_numbers = [int(i) for i in line_board_numbers]
            for j, x in enumerate(line_board_numbers):
                positions.append(Position(board_number, j, board_row, x))
        if board_row == 5:
            board_number += 1

    return (numbers,positions)

def is_board_won(board_positions, board_id):
    # check rows
    for y_id in range(5):
        row_positions = list(filter(lambda x : x.y_id == y_id and x.marked == True, board_positions))
        if row_positions.__len__() == 5:
            return True

    # check columns
    for x_id in range(5):
        column_positions = list(filter(lambda x : x.x_id == x_id and x.marked == True, board_positions))
        if column_positions.__len__() == 5:
            return True
    return False

def print_board_score(board_positions, number):
    unmarked = list(filter(lambda x : x.marked == False, board_positions))
    sum_unmarked = sum(position.value for position in unmarked)
    print(sum_unmarked * number)

def part_1(numbers, positions):
    board_count = int(positions.__len__() / 25)

    # draw number
    for number in numbers:
        # mark positions
        for position in positions:
            if position.value == number:
                position.marked = True

        # check for winner
        for board_id in range(board_count):
            board_positions = list(filter(lambda x : x.board_id == board_id, positions))
            if is_board_won(board_positions, board_id):
                print_board_score(board_positions, number)
                return

def part_2(numbers, positions):
    board_count = int(positions.__len__() / 25)
    board_ids_won = []

    # draw number
    for number in numbers:
        # mark positions
        for position in positions:
            if position.value == number:
                position.marked = True

        # check for winner
        for board_id in range(board_count):
            if board_ids_won.__contains__(board_id):
                continue

            board_positions = list(filter(lambda x : x.board_id == board_id, positions))
            if is_board_won(board_positions, board_id):
                if board_ids_won.__len__() == board_count - 1:
                    print_board_score(board_positions, number)
                    return
                board_ids_won.append(board_id)

numbers, positions = parse_input(lines)
part_1(numbers, copy.deepcopy(positions))
part_2(numbers, copy.deepcopy(positions))
