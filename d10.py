file = open("input/d10.txt")
lines = file.readlines()
file.close()

def part_1(lines):
    sum = 0
    opening_chars = ['(', '[', '{', '<']
    closing_chars = [')', ']', '}', '>']
    close_to_open_map = {')':'(', ']':'[', '}':'{', '>':'<'}
    points = {')': 3, ']': 57, '}': 1197, '>': 25137}

    for line in lines:
        stack = []

        for char in line:
            if char in opening_chars:
                stack.append(char)
            if char in closing_chars:
                open_char = close_to_open_map[char]
                last_char = stack.pop()
                if last_char != open_char:
                    sum += points[char]

    print(sum)

def part_2(lines):
    opening_chars = ['(', '[', '{', '<']
    closing_chars = [')', ']', '}', '>']
    close_to_open_map = {')':'(', ']':'[', '}':'{', '>':'<'}
    open_to_close_map = {'(':')', '[':']', '{':'}', '<':'>'}
    points = {')': 1, ']': 2, '}': 3, '>': 4}

    scores = []

    for line in lines:
        stack = []
        corrupted = False

        for char in line:
            if char in opening_chars:
                stack.append(char)
            if char in closing_chars:
                open_char = close_to_open_map[char]
                last_char = stack.pop()
                if last_char != open_char:
                    corrupted = True

        if not corrupted:
            score = 0
            reverse_stack = reversed(stack)
            for item in reverse_stack:
                score *= 5
                close_char = open_to_close_map[item]
                score += points[close_char]
            scores.append(score)

    scores.sort()
    middleIndex = int((scores.__len__() - 1)/2)
    print(scores[middleIndex])

part_1(lines)
part_2(lines)
