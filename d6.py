import copy

file = open("input/d6.txt")
lines = file.readlines()
file.close()

def parse_input(lines):
    numbers = lines.pop(0).split(',')
    numbers = [int(i) for i in numbers]
    return numbers

def calc_fish_on_day(school, day):
    fish_by_day_count = [0] * 9

    for fish in school:
        fish_by_day_count[fish] += 1

    for _ in range(day):
        new_count = [0] * 9
        for i in range(9):
            if i == 0:
                new_count[6] = fish_by_day_count[i]
                new_count[8] = fish_by_day_count[i]
            else:
                new_count[i-1] += fish_by_day_count[i]
        fish_by_day_count = new_count

    print(sum(fish_by_day_count))

def part_1(school):
    calc_fish_on_day(school, 80)


def part_2(school):
    calc_fish_on_day(school, 256)

school = parse_input(lines)
part_1(copy.deepcopy(school))
part_2(copy.deepcopy(school))
