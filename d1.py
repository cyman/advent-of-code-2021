file = open("input/d1.txt")
lines = file.readlines()
file.close()

numbers = [int(i) for i in lines]

def part_1():
    increases = 0
    for (i,number) in enumerate(numbers):
        if i >= 0 and numbers[i] > numbers[i-1]:
            increases += 1
    print(increases)

def part_2():
    increases = 0
    for (i,number) in enumerate(numbers):
        previous_window = numbers[i-3] + numbers[i-2] + numbers[i-1]
        current_window = numbers[i-2] + numbers[i-1] + numbers[i]
        if i >= 3 and previous_window < current_window:
            increases += 1
    print(increases)

part_1()
part_2()
