import copy

file = open("input/d13.txt")
lines = file.readlines()
file.close()

def parse_input(lines):
    locations = []
    folds = []

    end_of_locations = False
    for line in lines:
        if line.strip() == "":
            end_of_locations = True
            continue

        if not end_of_locations:
            a, b = line.strip().split(',')
            locations.append((int(a), int(b)))
        else:
            _, x = line.strip().split("fold along ")
            a, b = x.split('=')
            folds.append((a, int(b)))

    return (locations, folds)

def print_map(locations):
    highest_x = max([x for (x, y) in locations])
    highest_y = max([y for (x, y) in locations])
    for y in range(highest_y + 1):
        for x in range(highest_x + 1):
            if locations.__contains__((x, y)):
                print('#', end='')
            else:
                print('.', end='')
        print()


def fold_horizontally(locations, y_fold):
    to_remove = []
    to_add = []
    for x, y in locations:
        if y == y_fold:
            to_remove.append((x, y))
            continue
        if y > y_fold:
            to_add.append((x, y_fold - (y - y_fold)))
            to_remove.append((x, y))
            continue
    for item in to_remove:
        locations.remove(item)
    for item in to_add:
        locations.append(item)
    return list(set(locations))

def fold_vertically(locations, x_fold):
    to_remove = []
    to_add = []
    for x, y in locations:
        if x == x_fold:
            to_remove.append((x, y))
            continue
        if x > x_fold:
            to_add.append((x_fold - (x - x_fold), y))
            to_remove.append((x, y))
            continue
    for item in to_remove:
        locations.remove(item)
    for item in to_add:
        locations.append(item)
    return list(set(locations))

def part_1(locations, folds):
    fold_type, fold_location = folds[0]
    if fold_type == 'x':
        locations = fold_vertically(locations, fold_location)
    else:
        locations = fold_horizontally(locations, fold_location)
    print(locations.__len__())

def part_2(locations, folds):
    for fold_type, fold_location in folds:
        if fold_type == 'x':
            locations = fold_vertically(locations, fold_location)
        else:
            locations = fold_horizontally(locations, fold_location)
    print_map(locations)

locations, folds = parse_input(lines)
part_1(copy.deepcopy(locations), folds)
part_2(copy.deepcopy(locations), folds)
